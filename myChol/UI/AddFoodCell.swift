//
//  FoodSearchTableViewCell.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 29/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import UIKit

class AddFoodCell: UITableViewCell {
    
    @IBOutlet weak var satFat: UILabel!
    @IBOutlet weak var cholesterol: UILabel!
    @IBOutlet weak var foodName: UILabel!
    @IBOutlet weak var fiber: UILabel!
    
    func setExercise(food:FoodToday){
        foodName.text = food.name
        cholesterol.text = "Cholesterol " + String(food.cholesterol!) + "g"
        satFat.text = "Sat_fat " + String(food.sat_fat!) + "g"
        fiber.text = "Fiber " + String(food.fiber!) + "g"
        
    }
}
