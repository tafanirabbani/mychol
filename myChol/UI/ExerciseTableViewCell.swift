//
//  ExerciseTableViewCell.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 29/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import UIKit

class ExerciseTableViewCell: UITableViewCell {

    @IBOutlet weak var ExerciseTitle: UILabel!
 

    func setExercise(exercise:Exercise){
        ExerciseTitle.text = exercise.title
    }
}
