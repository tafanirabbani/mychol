//
//  FoodSearchTableViewCell.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 29/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import UIKit

class FoodSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var foodName: UILabel!
    
    func setExercise(food:SearchFood.Food){
        foodName.text = food.food_name
    }
}
