//
//  SearchFOod.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 27/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import Foundation
import CryptoSwift


struct FoodModel{
    static var food:GetFood.Food?
}
class GetFood {
    
    static var APP_METHOD : String = "GET"
    static var APP_KEY : String = "09119196e6b7403bb20f1413e071a12d"
    static var APP_SECRET : String = "97fa7a8378754424b11100c806749535&"
    static var APP_URL : String = "https://platform.fatsecret.com/rest/server.api"
    //    static var HMAC_SHA1_ALGORITHM : String = "HmacSHA1"
    
    //making random value
    private var nonce: String {
        get {
            var string: String = ""
            let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            let char = Array(letters)
            for _ in 1...7 { string.append(char[Int(arc4random()) % char.count]) }
            return string
        }
    }
    //timestamp needs to be * 2000
    private var timestamp: String {
        get { return String(Int(Date().timeIntervalSince1970*2000)) }
    }
    
    //generate string list of params
    private var generate0authParam : [String]{
        get{
            let s : [String] = [
                "oauth_consumer_key=" + SearchFood.APP_KEY,
                "oauth_signature_method=HMAC-SHA1",
                "oauth_timestamp=" + timestamp, // Should be  Long.valueOf(System.currentTimeMillis() / 1000).toString()
                "oauth_nonce=" + nonce,
                "oauth_version=1.0",
                "format=json",
                "page_number=" + "0",
                "max_results=" + "20"   ]
            return s
        }
    }
    
    //geting signature with HMAC.SHA1
    func getSignature(key: String, params: String) -> String {
        var array = [UInt8]()
        
        array += params.utf8
        
        let sign = try! HMAC(key: key, variant: .sha1).authenticate(array).toBase64()!
        
        return sign
    }
    
    public func getFood(id : String){
        var params :[String] = generate0authParam
        params.append("method=food.get")
        params.append("Food_id=" + id)
        var setring : String = ""
        params.sort()
        for i in 0...params.count-1{
            if i > 0{
                setring.append("&")
            }
            setring.append(params[i])
        }
        setring = setring.replacingOccurrences(of: "=", with: "%3D")
        setring = setring.replacingOccurrences(of: "&", with: "%26")
        
        var p : [String] = [SearchFood.APP_METHOD,SearchFood.APP_URL.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!,setring]
        
        var stringFromP : String = ""
        for i in 0...p.count-1{
            if i > 0{
                stringFromP.append("&")
            }
            stringFromP.append(p[i])
        }
        
        let s1 : String = getSignature(key: SearchFood.APP_SECRET, params: stringFromP)
        params.append("oauth_signature=" + s1)
        
        stringFromP = SearchFood.APP_URL + "?"
        for i in 0...params.count-1{
            if i > 0{
                stringFromP.append("&")
            }
            stringFromP.append(params[i])
        }
        
        let request = URLRequest(url: URL(string: String(describing: stringFromP).replacingOccurrences(of: "+", with: "%2B"))!)
        //prepare the calls
        let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
            guard let data = data else { return }
            do{
                guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers ) as? [String:Any] else {
                    return
                }
//                print(json)
                let foodDetail = json["food"] as? [String:Any]
//                print(foodDetail)
                let nutritions = foodDetail!["servings"] as? [String:Any]
//                let foodDesc = foodDetail
                let nutritionData = nutritions!["serving"] as? [Any]
                
                if nutritionData?.count == nil{
                    let detailNutrition = nutritions!["serving"] as? [String:Any]
                    //                print(detailNutrition)
//                    print(detailNutrition)
                    var serving:Servings = Servings()

                    serving.cholesterol = detailNutrition!["cholesterol"] as? String
                    serving.fiber = detailNutrition!["fiber"] as! String
                    serving.sat_fat = detailNutrition!["saturated_fat"] as! String
//                    serving.trans_fat = detailNutrition!["trans_fat"] as! String
                    //                print(serving)
                    //                print(foodDetail)
                    var foodDesc:Food = Food()
                    foodDesc.food_name = foodDetail!["food_name"]as! String
                    foodDesc.id = foodDetail!["food_id"]as! String
                    foodDesc.serving = serving
                    //                print(foodDesc)
                    FoodModel.food = foodDesc
                }else{
                    let detailNutrition = nutritionData?[1] as? [String:Any]
//                    print(detailNutrition)
                    
                    var serving:Servings = Servings()
                    
                    serving.cholesterol = detailNutrition!["cholesterol"] as? String ?? "0"
                    serving.fiber = detailNutrition!["fiber"] as? String ?? "0"
                    serving.sat_fat = detailNutrition!["saturated_fat"] as? String ?? "0"
                    serving.trans_fat = detailNutrition!["trans_fat"] as? String ?? "0"
                    //                print(serving)
                    //                print(foodDetail)
                    var foodDesc:Food = Food()
                    foodDesc.food_name = foodDetail!["food_name"]as! String
                    foodDesc.id = foodDetail!["food_id"]as! String
                    foodDesc.serving = serving
                    //                print(foodDesc)
                    FoodModel.food = foodDesc
                }
            }catch _{
                print("error")
            }
        }
        //starting the calls
        task.resume()
    }
    
    struct Food {
        var id: String?
        var food_name: String?
        var serving: Servings?
    }
    struct Servings {
        var cholesterol : String?
        var sat_fat : String?
        var trans_fat : String?
        var fiber : String?
    }
    
}
