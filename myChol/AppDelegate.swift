//
//  AppDelegate.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 23/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    enum Constants {
        static let apiKey = "09119196e6b7403bb20f1413e071a12d"
        static let apiSecret = "97fa7a8378754424b11100c806749535"
    }
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
                // Override point for customization after application launch.
        let realm = try! Realm()
        
//        let sc : SearchFood = SearchFood()
//        sc.searchFood(food : "mcDonald")
//        let sc : GetFood = GetFood()
//        sc.getFood(id: "25721")
//        print(FoodModel.food)
        
        let users = realm.objects(RealmUserModel.self)
        let photo = realm.objects(RealmPhotoProfile.self)
//        print(photo.count)
        if photo.count > 0{
//            print(photo.first?.path)
            PhotoProfilePath.path = photo.first!.path!
        }
//        print(users.count)
        if users.count > 0{
            
            let user = users.first
//            UserModel.initUser(user: user)
            initUser(user: user!)
            self.window = UIWindow(frame: UIScreen.main.bounds)
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "HomeIndentifier")
            
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        }else{
            self.window = UIWindow(frame: UIScreen.main.bounds)
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "WelcomeIdentifier")
            
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        }
        return true
    }

    func initUser(user: RealmUserModel){
        UserModel.username = user.username!
        UserModel.age = Int(user.age!)!
        UserModel.gender = user.gender!
        UserModel.smoker = Bool(user.smoker!)!
        UserModel.chdHistory = Bool(user.chdHistory!)!
        UserModel.bloodPressure1 = Int(user.bloodPressure1!)!
        UserModel.bloodPressure2 = Int(user.bloodPressure2!)!
        UserModel.ldlLevel = Int(user.ldlLevel!)!
        UserModel.hdlLevel = Int(user.hdlLevel!)!
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }


}

