//
//  HomePageController.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 25/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import UIKit
import RealmSwift
class HomePageController: UIViewController {

    
    @IBOutlet weak var fiber_percent_fiber: UILabel!
    @IBOutlet weak var cholesterol_percent_label: UILabel!
    @IBOutlet weak var saturated_percent_label: UILabel!
    @IBOutlet weak var monthButton: UIButton!
    @IBOutlet weak var date7: UILabel!
    @IBOutlet weak var date6: UILabel!
    @IBOutlet weak var date5: UILabel!
    @IBOutlet weak var date4: UILabel!
    @IBOutlet weak var date3: UILabel!
    @IBOutlet weak var date2: UILabel!
    @IBOutlet weak var date1: UILabel!
    @IBOutlet weak var name7: UILabel!
    @IBOutlet weak var name6: UILabel!
    @IBOutlet weak var name5: UILabel!
    @IBOutlet weak var name4: UILabel!
    @IBOutlet weak var name3: UILabel!
    @IBOutlet weak var name2: UILabel!
    @IBOutlet weak var name1: UILabel!
    @IBOutlet weak var progress: CircularProgressBar!
    @IBOutlet weak var progress3: CircularProgressBar!
    @IBOutlet weak var progress2: CircularProgressBar!

    var tanggal = [0,0,0,0,0,0,0]
    var day : Int = 0
    var dayInWeek : Int = 0
    var month : Int = 0
    var nMonth : Int = 0
    var year : Int = 0
    var hari : [String] = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]
    var bulan : [String] = ["January","Februari","Maret","April","Mei","Juni","July","Agustus","September","Oktober","November","Desember"]
    var date = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let calendar = Calendar.current
        let components = calendar.dateComponents([.month, .day, .weekday, .year], from: date)
        
        dayInWeek = components.weekday!
        month = components.month!
        nMonth = month
        day = components.day!
        year = components.year!
        updateDateModel()
        
        initCalender()
        
        addTapRecognizer()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let calendar = Calendar.current
        let components = calendar.dateComponents([.month, .day, .weekday, .year], from: date)
        
        dayInWeek = components.weekday!
        month = components.month!
        nMonth = month
        day = components.day!
        year = components.year!
        updateDateModel()
        
        initCalender()
        
        addTapRecognizer()
        
    }
    func findMonthMax(base:Int)->Int{
        
        let s = date1.text
        let age = Int(s!)
        
        if base == 2{
            if (year%4)==0{
                return 29
            }else{
                return 28
            }
        }else{
            if (base%2)==1{
                return 31
            }else{
                return 30
            }
        }
    }
    func getDateNum(base:Int,min:Int)->Int{
        
        let max = findMonthMax(base: month)
        if (base + min) > max{
            return base + min - max
        }else if (base + min) < 1{
            let a = month - 1
//            print(a)
            let max2 = findMonthMax(base: a)
//            print(max2)
//            max2 = max - 1
           return base + min + max2
        }else{
            return base + min
        }
        
    }
    func getdaynum(base:Int,min:Int)->Int{
        
        if (base-min)<0{
            
            return base-min+7
        }else if (base-min)>6{
            return base-min-7
        }else{
            return base-min
        }
    }
    func updateProgressBar(){
        
        var percertA = StaticCounter.chol/StaticCounter.maxCholesterol * 100
        let a = NSString(format: "%.1f", percertA) as String
        if percertA > 100{
            cholesterol_percent_label.textColor = UIColor.red
        }else{
            cholesterol_percent_label.textColor = UIColor.white
        }
        cholesterol_percent_label.text = a + "%"
        percertA = percertA * 360
        percertA = percertA / 100
//        print(percertA)
        if percertA > 360.0{
            percertA = 359
        }
        var percertB = StaticCounter.trans/StaticCounter.maxTrans * 100
        if percertB > 100{
            fiber_percent_fiber.textColor = UIColor.green
        }else{
            fiber_percent_fiber.textColor = UIColor.white
        }
        let b = NSString(format: "%.1f", percertB) as String
        fiber_percent_fiber.text = b + "%"
        percertB = percertB * 360
        percertB = percertB / 100
//        print(percertA)
        if percertB > 360.0{
            percertB = 359
        }
        var percertC = StaticCounter.sat/StaticCounter.maxSaturated * 100
        if percertC > 100{
            saturated_percent_label.textColor = UIColor.red
        }else{
            saturated_percent_label.textColor = UIColor.white
        }
        let c = NSString(format: "%.1f", percertC) as String
        saturated_percent_label.text = c + "%"
        percertC = percertC * 360
        percertC = percertC / 100
//        print(percertA)
        if percertC > 360.0{
            percertC = 359
        }
        progress.animate(toAngle: percertC, duration: 0.7, completion: nil)
        progress2.animate(toAngle: percertB, duration: 0.7, completion: nil)
        progress3.animate(toAngle: percertA, duration: 0.7, completion: nil)
    }
    func initCalender(){
        StaticCounter.chol = 0
        StaticCounter.trans = 0
        StaticCounter.sat = 0
        let realm = try! Realm()
        let foodlist = realm.objects(RealmFoodModel.self).filter("day = \(DateModel.date) AND month == \(DateModel.month) AND year == \(DateModel.year)")
        for  food in foodlist{
            StaticCounter.chol = StaticCounter.chol + Double(food.cholesterol!)!
            StaticCounter.trans = StaticCounter.trans + Double(food.fiber!)!
            StaticCounter.sat = StaticCounter.sat + Double(food.sat_fat!)!
        }
        updateProgressBar()
        
        tanggal[0] = getDateNum(base: day, min: -3)
        tanggal[1] = getDateNum(base: day, min: -2)
        tanggal[2] = getDateNum(base: day, min: -1)
        tanggal[3] = getDateNum(base: day, min: 0)
        tanggal[4] = getDateNum(base: day, min: 1)
        tanggal[5] = getDateNum(base: day, min: 2)
        tanggal[6] = getDateNum(base: day, min: 3)
//
        name1.text = hari[getdaynum(base: dayInWeek, min: 4)]
        name2.text = hari[getdaynum(base: dayInWeek, min: 3)]
        name3.text = hari[getdaynum(base: dayInWeek, min: 2)]
        name4.text = hari[getdaynum(base: dayInWeek, min: 1)]
        name5.text = hari[getdaynum(base: dayInWeek, min: 0)]
        name6.text = hari[getdaynum(base: dayInWeek, min: -1)]
        name7.text = hari[getdaynum(base: dayInWeek, min: -2)]
//
        dayInWeek = getdaynum(base: dayInWeek, min: 0)

        date1.text = String(tanggal[0])
        date2.text = String(tanggal[1])
        date3.text = String(tanggal[2])
        date4.text = String(tanggal[3])
        date5.text = String(tanggal[4])
        date6.text = String(tanggal[5])
        date7.text = String(tanggal[6])
       monthButton.setTitle(bulan[month-1], for: UIControl.State.normal)
//        day = tanggal[3]
    }
    
    
    func addTapRecognizer(){
        var tap = UITapGestureRecognizer(target: self, action: #selector(HomePageController.tapFunction7))
        date7.addGestureRecognizer(tap)
        date7.isUserInteractionEnabled = true
        tap = UITapGestureRecognizer(target: self, action: #selector(HomePageController.tapFunction6))
        date6.addGestureRecognizer(tap)
        date6.isUserInteractionEnabled = true
        tap = UITapGestureRecognizer(target: self, action: #selector(HomePageController.tapFunction5))
        date5.addGestureRecognizer(tap)
        date5.isUserInteractionEnabled = true
        tap = UITapGestureRecognizer(target: self, action: #selector(HomePageController.tapFunction4))
        date4.addGestureRecognizer(tap)
        date4.isUserInteractionEnabled = true
        tap = UITapGestureRecognizer(target: self, action: #selector(HomePageController.tapFunction3))
        date3.addGestureRecognizer(tap)
        date3.isUserInteractionEnabled = true
        tap = UITapGestureRecognizer(target: self, action: #selector(HomePageController.tapFunction2))
        date2.addGestureRecognizer(tap)
        date2.isUserInteractionEnabled = true
        tap = UITapGestureRecognizer(target: self, action: #selector(HomePageController.tapFunction1))
        date1.addGestureRecognizer(tap)
        date1.isUserInteractionEnabled = true
        
    }
    @objc func tapFunction6(sender:UITapGestureRecognizer) {
        let interval = TimeInterval(60 * 60 * 24 * 2)
        date = date.addingTimeInterval(interval)
        let calendar = Calendar.current
        let components = calendar.dateComponents([.month, .day, .weekday, .year], from: date)
        
        dayInWeek = components.weekday!
        month = components.month!
        nMonth = month
        day = components.day!
        year = components.year!
        updateDateModel()
        initCalender()
    }
    
    @objc func tapFunction5(sender:UITapGestureRecognizer) {
        
        let interval = TimeInterval(60 * 60 * 24 * 1)
        date = date.addingTimeInterval(interval)
        let calendar = Calendar.current
        let components = calendar.dateComponents([.month, .day, .weekday, .year], from: date)
        
        dayInWeek = components.weekday!
        month = components.month!
        nMonth = month
        day = components.day!
        year = components.year!
        updateDateModel()
        initCalender()
    }
    @objc func tapFunction4(sender:UITapGestureRecognizer) {
//        print("tap 4")
    }
    @objc func tapFunction3(sender:UITapGestureRecognizer) {
        
        let interval = TimeInterval(60 * 60 * 24 * (-1))
        date = date.addingTimeInterval(interval)
        let calendar = Calendar.current
        let components = calendar.dateComponents([.month, .day, .weekday, .year], from: date)
        
        dayInWeek = components.weekday!
        month = components.month!
        nMonth = month
        day = components.day!
        year = components.year!
        updateDateModel()
        initCalender()
    }
    @objc func tapFunction2(sender:UITapGestureRecognizer) {
//        print("tap 2")
//        day = day - 2
//        dayInWeek = dayInWeek - 2
//        initCalender()
        
        let interval = TimeInterval(60 * 60 * 24 * (-2))
        date = date.addingTimeInterval(interval)
        let calendar = Calendar.current
        let components = calendar.dateComponents([.month, .day, .weekday, .year], from: date)
        
        dayInWeek = components.weekday!
        month = components.month!
        nMonth = month
        day = components.day!
        year = components.year!
        updateDateModel()
        initCalender()
    }
    @objc func tapFunction1(sender:UITapGestureRecognizer) {
//        print("tap 1")
//        day = day - 3
//        dayInWeek = dayInWeek - 3
//        initCalender()
        
        let interval = TimeInterval(60 * 60 * 24 * (-3))
        date = date.addingTimeInterval(interval)
        let calendar = Calendar.current
        let components = calendar.dateComponents([.month, .day, .weekday, .year], from: date)
        
        dayInWeek = components.weekday!
        month = components.month!
        nMonth = month
        day = components.day!
        year = components.year!
        updateDateModel()
        initCalender()
    }
    @objc func tapFunction7(sender:UITapGestureRecognizer) {
//        day = day + 3
//        dayInWeek = dayInWeek + 3
//        initCalender()
        let interval = TimeInterval(60 * 60 * 24 * 3)
        date = date.addingTimeInterval(interval)
        let calendar = Calendar.current
        let components = calendar.dateComponents([.month, .day, .weekday, .year], from: date)
        
        dayInWeek = components.weekday!
        month = components.month!
        nMonth = month
        day = components.day!
        year = components.year!
        updateDateModel()
        initCalender()
    }
    @IBOutlet weak var datePickerView: UIDatePicker!
    
    @IBAction func monthPick(_ sender: Any) {
//        datePickerView.backgroundColor = UIColor.white
//        datePickerView.datePickerMode = .date
//        datePickerView.date = date
//        datePickerView.isHidden = false
      
    }
    
    @IBAction func nextWeek(_ sender: Any) {
        let interval = TimeInterval(60 * 60 * 24 * 7)
        date = date.addingTimeInterval(interval)
        let calendar = Calendar.current
        let components = calendar.dateComponents([.month, .day, .weekday, .year], from: date)
        
        dayInWeek = components.weekday!
        month = components.month!
        nMonth = month
        day = components.day!
        year = components.year!
        updateDateModel()
        initCalender()
    }
    
    @IBAction func prevWeek(_ sender: Any) {
        let interval = TimeInterval(60 * 60 * 24 * (-7))
        date = date.addingTimeInterval(interval)
        let calendar = Calendar.current
        let components = calendar.dateComponents([.month, .day, .weekday, .year], from: date)
        dayInWeek = components.weekday!
        month = components.month!
        nMonth = month
        day = components.day!
        year = components.year!
        updateDateModel()
        initCalender()
    }
    @IBAction func datePicking(_ sender: Any) {
    }
    func updateDateModel(){
        DateModel.date = day
        DateModel.month = month
        DateModel.year = year
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "add_breakfast"{
            FoodDetailModel.type = "breakfast"
        }else if segue.identifier == "add_lunch"{
            FoodDetailModel.type = "lunch"
        }else if segue.identifier == "add_dinner"{
            FoodDetailModel.type = "dinner"
        }else{
//            print("error")
        }
    }
}

