//
//  detailProfile.swift
//  myChol
//
//  Created by Malvin Santoso on 26/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import UIKit

class detailProfile: UIViewController {
    
    @IBOutlet var detailprofileView: UIView!
    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var detailTextView: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        detailTextView.text = DetailData.detail
        self.title = DetailData.titleDetail
        detailImageView.image = UIImage(named: DetailData.namaGambar)
    }
    
    
}
