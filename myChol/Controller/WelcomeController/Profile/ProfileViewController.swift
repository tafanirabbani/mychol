//
//  ProfileViewController.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 26/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import UIKit
import RealmSwift
class ProfileViewController: UIViewController , UIImagePickerControllerDelegate , UINavigationControllerDelegate , UITextFieldDelegate{
    
    //    @IBOutlet weak var hdlView: UILabel!
    //    @IBOutlet weak var ldlView: UILabel!
    //    @IBOutlet weak var bpView: UILabel!
    //    @IBOutlet weak var chdView: UILabel!
    //    @IBOutlet weak var smokerView: UILabel!
    //    @IBOutlet weak var genderView: UILabel!
    
    @IBOutlet weak var nameVIew: UILabel!
    //    @IBOutlet weak var ageView: UILabel!
    @IBOutlet weak var ldlStatus: UIButton!
    @IBOutlet weak var bloodStatus: UIButton!
    @IBOutlet weak var primaryRiskStatus: UIButton!
    @IBOutlet weak var tenYearStatus: UIButton!
    @IBOutlet weak var ageView: UITextField!
    @IBOutlet weak var ldlView: UITextField!
    @IBOutlet weak var hdlView: UITextField!
    @IBOutlet weak var genderView: UISegmentedControl!
    @IBOutlet weak var smokerView: UISegmentedControl!
    @IBOutlet weak var chdView: UISegmentedControl!
    @IBOutlet weak var bp1View: UITextField!
    @IBOutlet weak var bp2View: UITextField!
    
    @IBOutlet weak var gambarProfilepicture: UIImageView!
    
    let image = UIImagePickerController()
    
    @IBOutlet weak var usernameLabel: UILabel!
    var username : String?
    var age : Int?
    var gender : String?
    var smoker : Bool = false
    var CHDHistory : Bool = false
    var bloodp1 : Int?
    var bloodp2 : Int?
    var ldlLevel : Int?
    var hdlLevel : Int?
    var primaryRiks : Int = 0
    var tenYearScore : Int = 0
    var tampungGender:Int = 0
    var tampungSmoker:Int = 0
    var tampungChd:Int = 0
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ldlSegue"{
            DetailData.titleDetail = "LDL Level"
            DetailData.namaGambar = "fat2_img" //namaGambarnya
            DetailData.detail = """
            LDL (low density-lipoprotein) cholesterol is also called "bad" cholesterol.
            
            LDL can build up on the walls of your arteries and increase your chances of getting heart disease.
            
            If you do not have heart or blood vessel disease and are not at high risk for developing heart disease, the following guidelines apply.
            """
        }else if segue.identifier == "bloodPressure"{
            DetailData.titleDetail = "Blood Pressure"
            DetailData.namaGambar = "fat2_img" //namaGambarnya
            DetailData.detail = """
            The pressure of the blood in the circulatory system, often measured for diagnosis since it is closely related to the force and rate of the heartbeat and the diameter and elasticity of the arterial walls.
            "heart failure is common in patients with high blood pressure".
            """
        }else if segue.identifier == "primaryRisk"{
            DetailData.titleDetail = "Primay Risk Factor"
            DetailData.namaGambar = "fat2_img" //namaGambarnya
            DetailData.detail =  """
            1. Age, ≥45 years (men) and ≥ 55 years or postmenopausal in women.
            
            2. Family history of premature CHD (definite myocardial infarction or sudden death), before age 55 in a first-degree male relative or before age 65 in a first-degree female relative.
            
            3. Cigarette smoking.
            
            4. Hypertension, 140/90 mmHg and above or taking antihypertensive medication.
            """
            
        }else if segue.identifier == "10yearRisk"{
            DetailData.titleDetail = "10 Year Risk Assessment"
            DetailData.namaGambar = "fat2_img" //namaGambarnya
            DetailData.detail = """
            10-year risk for developing cardiovascular disease.
            """
        }else{
            print("error")
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let image2 = UIImage(contentsOfFile: PhotoProfilePath.path)
        gambarProfilepicture.image = image2
        
        self.hideKeyboardWhenTappedAround()
        rubahKeInt()
        initProfile()
        gambarProfilepicture.layer.cornerRadius = gambarProfilepicture.frame.width / 2
        gambarProfilepicture.clipsToBounds = true
        if gender == "Male"{
            tenYearScore = tenYearCountMale(ldlLevel: ldlLevel!, hdlLevel: hdlLevel!, age: age!, smoker: smoker, bloodp1: bloodp1!)
        }else if gender == "Female"{
            tenYearScore = tenYearCountFemale(ldlLevel: ldlLevel!, hdlLevel: hdlLevel!, age: age!, smoker: smoker, bloodp1: bloodp1!)
        }
        //INI GATAU ERROR KENAPA TAFA YG COUNTPRIMARY RISK SM UPDATE BUTTON KALO DI COMMAND
        countPrimaryRisk()
        updateButton()
    }
    
    //Show alert
    func showAlert() {
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .camera)
        }))
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //get image from source type
    func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {
        
        //Check is source type available
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            
            let image = UIImagePickerController()
            image.delegate = self
            image.sourceType = sourceType
            self.present(image, animated: true, completion: nil)
        }
    }
    
    @IBAction func changeProfilePic(_ sender: Any) {
        showAlert()
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//        print(info[.imageURL])
        let s = info[.imageURL] as! NSURL
//        print(s)
//        print(s.path)
        let realm = try! Realm()
//        let photo = RealmPhotoProfile()
//        photo.path = s.path
        // Persist your data easily
        let photo = realm.objects(RealmPhotoProfile.self)
        if photo.count > 0{
         print(photo.count)
            DispatchQueue(label: "background").async {
                autoreleasepool {
                    let realm = try! Realm()
                    let users = realm.objects(RealmPhotoProfile.self)
                    try! realm.write {
                        users.first?.path = s.path
                    }
                }
            }
        }else{
            let photo = RealmPhotoProfile()
            photo.path = s.path
            try! realm.write {
                realm.add(photo)
            }
        }
        
        let image2 = UIImage(contentsOfFile: s.path!)
        gambarProfilepicture.image = image2
        
        
        self.dismiss(animated: true)
    }
    
    @IBAction func updateAge(_ sender: Any) {
        if ageView.text!.count > 2{
            ageView.text = "99"
            age = 99
        }else if ageView.text == "0"{
            ageView.text = "1"
            age = 1
        }
        UserModel.age = Int(ageView.text!)!
        age = Int(ageView.text!)!
//        let realm = try! Realm()
        DispatchQueue(label: "background").async {
            autoreleasepool {
                let realm = try! Realm()
                let users = realm.objects(RealmUserModel.self)
                try! realm.write {
                    users.first?.age = self.ageView?.text!
                }
            }
        }

        countPrimaryRisk()
        updateButton()
    }
    @IBAction func updateGender(_ sender: Any) {
        if genderView.selectedSegmentIndex == 0{
            UserModel.gender = "Male"
            gender = "Male"
        }else{
            UserModel.gender = "Female"
            gender = "Female"
        }
        DispatchQueue(label: "background").async {
            autoreleasepool {
                let realm = try! Realm()
                let users = realm.objects(RealmUserModel.self)
                try! realm.write {
                    users.first?.gender = self.gender!
                }
            }
        }
        countPrimaryRisk()
        updateButton()
    }
    @IBAction func updateSmoker(_ sender: Any) {
        if smokerView.selectedSegmentIndex == 0{
            UserModel.smoker = true
            smoker = true
        }else{
            UserModel.smoker = false
            smoker = false
        }
        DispatchQueue(label: "background").async {
            autoreleasepool {
                let realm = try! Realm()
                let users = realm.objects(RealmUserModel.self)
                try! realm.write {
                    users.first?.smoker = String(self.smoker)
                }
            }
        }
        countPrimaryRisk()
        updateButton()
    }
    @IBAction func updateChd(_ sender: Any) {
        if chdView.selectedSegmentIndex == 0{
            UserModel.chdHistory = true
            CHDHistory = true
        }else{
            UserModel.chdHistory = false
            CHDHistory = false
        }
        DispatchQueue(label: "background").async {
            autoreleasepool {
                let realm = try! Realm()
                let users = realm.objects(RealmUserModel.self)
                try! realm.write {
                    users.first?.chdHistory = String(self.CHDHistory)
                }
            }
        }
        countPrimaryRisk()
        updateButton()
    }
    @IBAction func updateBp1(_ sender: Any) {
        if bp1View.text!.isEmpty{
            UserModel.bloodPressure1 = 0
            bp1View.text = "None"
            bloodp1 = 0
        }
        if bp1View.text == "None"{
            UserModel.bloodPressure1 = 0
            bloodp1 = 0
        }else{
            UserModel.bloodPressure1 = Int(bp1View.text!)!
            bloodp1 = Int(bp1View.text!)!
        }
        DispatchQueue(label: "background").async {
            autoreleasepool {
                let realm = try! Realm()
                let users = realm.objects(RealmUserModel.self)
                try! realm.write {
                    users.first?.bloodPressure1 = String(self.bloodp1!)
                }
            }
        }
        countPrimaryRisk()
        updateButton()
    }
    @IBAction func updateBp2(_ sender: Any) {
        if bp2View.text!.isEmpty{
            UserModel.bloodPressure2 = 0
            bp2View.text = "None"
            bloodp2 = 0
        }
        if bp2View.text == "None"{
            UserModel.bloodPressure2 = 0
            bloodp2 = 0
        }else{
            UserModel.bloodPressure2 = Int(bp2View.text!)!
            bloodp2 = Int(bp2View.text!)!
        }
        DispatchQueue(label: "background").async {
            autoreleasepool {
                let realm = try! Realm()
                let users = realm.objects(RealmUserModel.self)
                try! realm.write {
                    users.first?.bloodPressure2 = String(self.bloodp2!)
                }
            }
        }
        countPrimaryRisk()
        updateButton()
        
    }
    @IBAction func updateLdl(_ sender: Any) {
        if ldlView.text!.isEmpty{
            UserModel.ldlLevel = 0
            ldlView.text = "None"
            ldlLevel = 0
        }
        if ldlView.text == "None"{
            UserModel.ldlLevel = 0
            ldlLevel = 0
        }else{
            UserModel.ldlLevel = Int(ldlView.text!)!
            ldlLevel = Int(ldlView.text!)!
        }
        DispatchQueue(label: "background").async {
            autoreleasepool {
                let realm = try! Realm()
                let users = realm.objects(RealmUserModel.self)
                try! realm.write {
                    users.first?.ldlLevel = String(self.ldlLevel!)
                }
            }
        }
        countPrimaryRisk()
        updateButton()
    }
    @IBAction func updateHdl(_ sender: Any) {
        if hdlView.text!.isEmpty{
            UserModel.hdlLevel = 0
            hdlView.text = "None"
            hdlLevel = 0
        }
        if hdlView.text == "None"{
            UserModel.hdlLevel = 0
            hdlLevel = 0
        }else{
            UserModel.hdlLevel = Int(hdlView.text!)!
            hdlLevel = Int(hdlView.text!)!
        }
        DispatchQueue(label: "background").async {
            autoreleasepool {
                let realm = try! Realm()
                let users = realm.objects(RealmUserModel.self)
                try! realm.write {
                    users.first?.hdlLevel = String(self.hdlLevel!)
                }
            }
        }
        countPrimaryRisk()
        updateButton()
    }
    
    func updateButton(){
        updateLDLButton()
        updateBloodButton()
        updatePrimaryRiskButton()
        updateTenYearButton()
    }
    func rubahKeInt(){
        if UserModel.gender == "Male"{
            tampungGender = 0
        }else{
            tampungGender = 1
        }
        if UserModel.smoker == true{
            tampungSmoker = 0
        }else{
            tampungSmoker = 1
        }
        if UserModel.chdHistory == true{
            tampungChd = 0
        }else{
            tampungChd = 1
        }
    }
    func initProfile(){
        ageView.text = "\(Int(UserModel.age))"
        genderView.selectedSegmentIndex = tampungGender
        smokerView.selectedSegmentIndex = tampungSmoker
        chdView.selectedSegmentIndex = tampungChd
        bp1View.text = "\(Int(UserModel.bloodPressure1 ))"
        bp2View.text = "\(Int(UserModel.bloodPressure2 ))"
        ldlView.text = "\(Int(UserModel.ldlLevel))"
        hdlView.text = "\(Int(UserModel.hdlLevel))"
        usernameLabel.text = UserModel.username
        age = UserModel.age
        gender = UserModel.gender
        smoker = UserModel.smoker
        CHDHistory = UserModel.chdHistory
        bloodp1 = UserModel.bloodPressure1
        bloodp2 = UserModel.bloodPressure2
        ldlLevel = UserModel.ldlLevel
        hdlLevel = UserModel.hdlLevel
        usernameLabel.text = UserModel.username
        
    }
    func updateTenYearButton( ){
        if tenYearScore > 20{
            tenYearStatus.setTitle("   Your 10-year  Risks Assesment             \(tenYearScore)%", for: UIControl.State.normal)
            tenYearStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            tenYearStatus.setBackgroundImage(UIImage(named: "redButton"), for: UIControl.State.normal)
        }else if tenYearScore > 10{
            tenYearStatus.setTitle("   Your 10-year  Risks Assesment              \(tenYearScore)%", for: UIControl.State.normal)
            tenYearStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            tenYearStatus.setBackgroundImage(UIImage(named: "yellowButton"), for: UIControl.State.normal)
        }else{
            tenYearStatus.setTitle("   Your 10-year  Risks Assesment              \(tenYearScore)%", for: UIControl.State.normal)
            tenYearStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            tenYearStatus.setBackgroundImage(UIImage(named: "square_btn"), for: UIControl.State.normal)
        }
    }
    
    func updateBloodButton()  {
        if (bloodp1! > 130) || (bloodp2! > 79 && bloodp2!<91){
            
            bloodStatus.setTitle("     Your Blood  Pressure Stage is              High", for: UIControl.State.normal)
            bloodStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            bloodStatus.setBackgroundImage(UIImage(named: "redButton"), for: UIControl.State.normal)
        } else if (bloodp1! > 119) && (bloodp2! < 80){
            bloodStatus.setTitle("     Your Blood  Pressure Stage is              Elevated", for: UIControl.State.normal)
            bloodStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            bloodStatus.setBackgroundImage(UIImage(named: "yellowButton"), for: UIControl.State.normal)
        }else if bloodp1 == 0 || bloodp2 == 0{
            bloodStatus.setTitle("     Your Blood  Pressure Stage is              None", for: UIControl.State.normal)
            bloodStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            bloodStatus.setBackgroundImage(UIImage(named: "greyButton"), for: UIControl.State.normal)
        }else {
            bloodStatus.setTitle("     Your Blood  Pressure Stage is              Good", for: UIControl.State.normal)
            bloodStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            bloodStatus.setBackgroundImage(UIImage(named: "square_btn"), for: UIControl.State.normal)
        }
    }
    func updatePrimaryRiskButton(){
        if primaryRiks == 1{
            primaryRiskStatus.setTitle("Your Primary Risk              is                1", for: UIControl.State.normal)
            primaryRiskStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            primaryRiskStatus.setBackgroundImage(UIImage(named: "yellowButton"), for: UIControl.State.normal)
        }else if primaryRiks > 1{
            primaryRiskStatus.setTitle("Your Primary Risk              is                \(primaryRiks)", for: UIControl.State.normal)
            primaryRiskStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            primaryRiskStatus.setBackgroundImage(UIImage(named: "redButton"), for: UIControl.State.normal)
        }else{
            primaryRiskStatus.setTitle("Your Primary Risk              is                \(primaryRiks)", for: UIControl.State.normal)
            primaryRiskStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            primaryRiskStatus.setBackgroundImage(UIImage(named: "square_btn"), for: UIControl.State.normal)
        }
    }
    func updateLDLButton(){
        if ldlLevel! > 189{
            ldlStatus.setTitle("Your LDL Level is              High", for: UIControl.State.normal)
            ldlStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            ldlStatus.setBackgroundImage(UIImage(named: "redButton"), for: UIControl.State.normal)
        }else if ldlLevel! > 159{
            ldlStatus.setTitle("Your LDL Level is    Borderline High", for: UIControl.State.normal)
            ldlStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            ldlStatus.setBackgroundImage(UIImage(named: "yellowButton"), for: UIControl.State.normal)
        }else if ldlLevel! > 0{
            ldlStatus.setTitle("Your LDL Level is          Normal", for: UIControl.State.normal)
            ldlStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            ldlStatus.setBackgroundImage(UIImage(named: "square_btn"), for: UIControl.State.normal)
        }else if ldlLevel! == 0{
            ldlStatus.setTitle("Your LDL Level is              None", for: UIControl.State.normal)
            ldlStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            ldlStatus.setBackgroundImage(UIImage(named: "greyButton"), for: UIControl.State.normal)
        }
        
    }
    func countPrimaryRisk(){
        primaryRiks = 0
        switch gender {
        case "Male":
            if age!>45{
                primaryRiks = primaryRiks + 1
            }
        case "Female":
            if age!>55{
                primaryRiks = primaryRiks + 1
            }
        default: break
        }
        if smoker{
            primaryRiks = primaryRiks + 1
        }
        if CHDHistory{
            primaryRiks = primaryRiks + 1
        }
        if bloodp1 == nil{
            bloodp1 = 0
        }
        if bloodp2 == nil {
            bloodp2 = 0
        }
        
        if (bloodp1!>130)&&(bloodp2!>80){
            primaryRiks = primaryRiks + 1
        }
    }
    
    func tenYearCountMale(ldlLevel:Int,hdlLevel:Int,age:Int,smoker:Bool,bloodp1:Int) -> Int{
        
        var point = 0
        let total = ldlLevel + hdlLevel
        var use = 0
        if age > 75{
            use = 0
        }else if age > 70 {
            use = 1
        }else if age>65{
            use = 2
        }else if age>60{
            use = 3
        }else if age>55{
            use = 4
        }else if age>50{
            use = 5
        }else if age>45{
            use = 6
        }else if age>40{
            use = 7
        }else if age>35{
            use = 8
        }else{
            use = 9
        }
        
        
        switch use {
        case 0:
            point = point + 13
            if total > 240{
                point = point + 1
            }
            if smoker{
                point = point + 1
            }
        case 1:
            point = point + 12
            if total > 240{
                point = point + 1
            }
            if smoker{
                point = point + 1
            }
        case 2:
            point = point + 11
            if total > 280{
                point = point + 3
            }else if total > 240{
                point = point + 2
            }else if total > 200{
                point = point + 1
            }else if total > 160{
                point = point + 1
            }
            
            if smoker{
                point = point + 1
            }
        case 3:
            point = point + 10
            if total > 280{
                point = point + 3
            }else if total > 240{
                point = point + 2
            }else if total > 200{
                point = point + 1
            }else if total > 160{
                point = point + 1
            }
            
            if smoker{
                point = point + 1
            }
        case 4:
            point = point + 8
            if total > 280{
                point = point + 5
            }else if total > 240{
                point = point + 4
            }else if total > 200{
                point = point + 3
            }else if total > 160{
                point = point + 2
            }
            if smoker{
                point = point + 3
            }
        case 5:
            point = point + 6
            if total > 280{
                point = point + 5
            }else if total > 240{
                point = point + 4
            }else if total > 200{
                point = point + 3
            }else if total > 160{
                point = point + 2
            }
            
            if smoker{
                point = point + 3
            }
        case 6:
            point = point + 3
            if total > 280{
                point = point + 8
            }else if total > 240{
                point = point + 6
            }else if total > 200{
                point = point + 5
            }else if total > 160{
                point = point + 3
            }
            if smoker{
                point = point + 5
            }
        case 7:
            point = point + 0
            point = point + 3
            if total > 280{
                point = point + 8
            }else if total > 240{
                point = point + 6
            }else if total > 200{
                point = point + 5
            }else if total > 160{
                point = point + 3
            }
            
            if smoker{
                point = point + 5
            }
        case 8:
            point = point - 4
            if total > 280{
                point = point + 11
            }else if total > 240{
                point = point + 7
            }else if total > 200{
                point = point + 4
            }else if total > 160{
                point = point + 0
            }
            if smoker{
                point = point + 8
            }
        case 9:
            point = point - 9
            if total > 280{
                point = point + 11
            }else if total > 240{
                point = point + 7
            }else if total > 200{
                point = point + 4
            }else if total > 160{
                point = point + 0
            }
            if smoker{
                point = point + 8
            }
        default:
            break
        }
        
        if hdlLevel > 60{
            point = point - 1
        }else if hdlLevel > 50{
            
        }else if hdlLevel > 40{
            point = point + 1
        }else{
            point = point + 2
        }
        if bloodp1 > 160{
            point = point + 2
        }else if bloodp1 > 130{
            point = point + 1
        }
        if point > 16{
            return 30
        } else if point > 15{
            return 25
        }else if point > 14{
            return 20
        }else if point > 13{
            return 16
        }else if point > 12{
            return 12
        }else if point > 11{
            return 10
        }else if point > 10{
            return 8
        }else if point > 9{
            return 6
        }else if point > 8{
            return 5
        }else if point > 7{
            return 4
        }else if point > 6{
            return 3
        }else if point > 4{
            return 2
        }else {
            return 1
        }
    }
    func tenYearCountFemale(ldlLevel : Int, hdlLevel : Int, age : Int?, smoker : Bool, bloodp1 : Int) -> Int{
        var point = 0
        
        let total = ldlLevel + hdlLevel
        var use = 0
        if age! > 75{
            use = 0
        }else if age! > 70 {
            use = 1
        }else if age!>65{
            use = 2
        }else if age!>60{
            use = 3
        }else if age!>55{
            use = 4
        }else if age!>50{
            use = 5
        }else if age!>45{
            use = 6
        }else if age!>40{
            use = 7
        }else if age!>35{
            use = 8
        }else{
            use = 9
        }
        
        
        switch use {
        case 0:
            point = point + 13
            if total > 240{
                point = point + 1
            }
            if smoker{
                point = point + 1
            }
        case 1:
            point = point + 12
            if total > 240{
                point = point + 1
            }
            if smoker{
                point = point + 1
            }
        case 2:
            point = point + 11
            if total > 280{
                point = point + 3
            }else if total > 240{
                point = point + 2
            }else if total > 200{
                point = point + 1
            }else if total > 160{
                point = point + 1
            }
            
            if smoker{
                point = point + 1
            }
        case 3:
            point = point + 10
            if total > 280{
                point = point + 3
            }else if total > 240{
                point = point + 2
            }else if total > 200{
                point = point + 1
            }else if total > 160{
                point = point + 1
            }
            
            if smoker{
                point = point + 1
            }
        case 4:
            point = point + 8
            if total > 280{
                point = point + 5
            }else if total > 240{
                point = point + 4
            }else if total > 200{
                point = point + 3
            }else if total > 160{
                point = point + 2
            }
            if smoker{
                point = point + 3
            }
        case 5:
            point = point + 6
            if total > 280{
                point = point + 5
            }else if total > 240{
                point = point + 4
            }else if total > 200{
                point = point + 3
            }else if total > 160{
                point = point + 2
            }
            
            if smoker{
                point = point + 3
            }
        case 6:
            point = point + 3
            if total > 280{
                point = point + 8
            }else if total > 240{
                point = point + 6
            }else if total > 200{
                point = point + 5
            }else if total > 160{
                point = point + 3
            }
            if smoker{
                point = point + 5
            }
        case 7:
            point = point + 0
            point = point + 3
            if total > 280{
                point = point + 8
            }else if total > 240{
                point = point + 6
            }else if total > 200{
                point = point + 5
            }else if total > 160{
                point = point + 3
            }
            
            if smoker{
                point = point + 5
            }
        case 8:
            point = point - 4
            if total > 280{
                point = point + 11
            }else if total > 240{
                point = point + 7
            }else if total > 200{
                point = point + 4
            }else if total > 160{
                point = point + 0
            }
            if smoker{
                point = point + 8
            }
        case 9:
            point = point - 9
            if total > 280{
                point = point + 11
            }else if total > 240{
                point = point + 7
            }else if total > 200{
                point = point + 4
            }else if total > 160{
                point = point + 0
            }
            if smoker{
                point = point + 8
            }
        default:
            break
        }
        
        if hdlLevel > 60{
            point = point - 1
        }else if hdlLevel > 50{
            
        }else if hdlLevel > 40{
            point = point + 1
        }else{
            point = point + 2
        }
        if bloodp1 > 160{
            point = point + 2
        }else if bloodp1 > 130{
            point = point + 1
        }
        if point > 16{
            return 30
        } else if point > 15{
            return 25
        }else if point > 14{
            return 20
        }else if point > 13{
            return 16
        }else if point > 12{
            return 12
        }else if point > 11{
            return 10
        }else if point > 10{
            return 8
        }else if point > 9{
            return 6
        }else if point > 8{
            return 5
        }else if point > 7{
            return 4
        }else if point > 6{
            return 3
        }else if point > 4{
            return 2
        }else {
            return 1
        }
    }
}
extension ProfileViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
