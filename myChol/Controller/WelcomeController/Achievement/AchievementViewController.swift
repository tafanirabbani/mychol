    //
    //  AchievementViewController.swift
    //  myChol
    //
    //  Created by Muhammad Tafani Rabbani on 26/04/19.
    //  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
    //
    
    import UIKit
    import RealmSwift
    class AchievementViewController: UIViewController {
        
        @IBOutlet weak var cholesterolButton: UIButton!
        @IBOutlet weak var workoutButton: UIButton!
        @IBOutlet weak var fiberButton: UIButton!
        var day : Int = 0
        var baseday : Int = 0
        var lastDay : Int = 0
        var dayInWeek : Int = 0
        var month : Int = 0
        var nMonth : Int = 0
        var year : Int = 0
        var totalExercise : Int = 0
        var totalFiber : Double = 0.0
        var totalCholesterol :Double = 0.0
        override func viewDidLoad() {
            super.viewDidLoad()
            
            
            let date = Date()
            let calendar = Calendar.current
            let components = calendar.dateComponents([.month, .day, .weekday, .year], from: date)
            
            dayInWeek = components.weekday!
//            print(dayInWeek)
            day = components.day!
            baseday = day - (dayInWeek-2)
            lastDay = baseday + 6
            
            let realm = try! Realm()
            let foodlist = realm.objects(RealmFoodModel.self).filter("day > \(baseday) AND day < \(lastDay)")
            for makan in foodlist{
                totalFiber = totalFiber + Double(makan.fiber!)!
                totalCholesterol = totalCholesterol + Double(makan.cholesterol!)!
                
            }
            let exerciseList = realm.objects(RealmExerciseModel.self).filter("day > \(baseday-1) AND day < \(lastDay+1)")
//            print(exerciseList)
            totalExercise = exerciseList.count + 1
            updateButton()
            // Do any additional setup after loading the view.
        }
        override func viewWillAppear(_ animated: Bool) {
            let date = Date()
            let calendar = Calendar.current
            let components = calendar.dateComponents([.month, .day, .weekday, .year], from: date)
            
            dayInWeek = components.weekday!
            //            print(dayInWeek)
            day = components.day!
            baseday = day - (dayInWeek-2)
            lastDay = baseday + 6
            print(baseday)
            let realm = try! Realm()
            let foodlist = realm.objects(RealmFoodModel.self).filter("day > \(baseday-1) AND day < \(lastDay+1)")
            for makan in foodlist{
                totalFiber = totalFiber + Double(makan.fiber!)!
                totalCholesterol = totalCholesterol + Double(makan.cholesterol!)!
                
            }
            
            let exerciseList = realm.objects(RealmExerciseModel.self).filter("day > \(baseday-1) AND day < \(lastDay+1)")
                        print("realm",exerciseList.count)
            totalExercise = exerciseList.count
            updateButton()
        }
        func updateButton(){
            print(totalExercise)
            if totalExercise > 2{
//                UIImage(named: "square_btn"), for: UIControl.State.normal
                workoutButton.setBackgroundImage(UIImage(named: "workout2_img-1"), for: UIControl.State.normal)
            }else{
                 workoutButton.setBackgroundImage(UIImage(named: "workout_img-1"), for: UIControl.State.normal)
            }
            if totalFiber/7 > 19{
                fiberButton.setBackgroundImage(UIImage(named: "fruit2_img-1"), for: UIControl.State.normal)
            }else{
                fiberButton.setBackgroundImage(UIImage(named: "fruit_img-1"), for: UIControl.State.normal)
            }
            if totalCholesterol/7 < 200{
                cholesterolButton.setBackgroundImage(UIImage(named: "chol2_img-1"), for: UIControl.State.normal)
            }else{
                cholesterolButton.setBackgroundImage(UIImage(named: "chol_img-1"), for: UIControl.State.normal)
            }
        }
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.identifier == "chol"{
                AchievementModel.ImageName = "chol3_img"
                AchievementModel.detail = """
                Reduce dietary cholesterol to less than 200 mg/day.
                """
                
            }
            else if segue.identifier == "workout"{
                AchievementModel.ImageName = "workout3_img"
                AchievementModel.detail = """
                Increase aerobic physical activity up to three times weekly or more.
                """
            }
            else if segue.identifier == "fruit"
            {
                AchievementModel.ImageName = "fruit3_img"
                AchievementModel.detail = """
                Use whole grains, fruits & vegetables as much as possible and at least 20 grams of dietary fiber per day.
                """
            }
            else if segue.identifier ==
                "fish"
            {
                AchievementModel.ImageName = "fish3_img"
                AchievementModel.detail = """
                Modify diet to include additional fiber, soy, fish, and reduced alcohol
                """
            }
            else if segue.identifier ==
                "fat"
            {
                AchievementModel.ImageName = "fat3_img"
                AchievementModel.detail = """
                Reduce saturated fat to less than 7% of dietary calories.
                """
            }
        }
        
        
    }
