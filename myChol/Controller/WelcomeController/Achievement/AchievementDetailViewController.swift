//
//  AchievementDetailViewController.swift
//  myChol
//
//  Created by Dionisius Ario Nugroho on 26/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import UIKit

class AchievementDetailViewController: UIViewController {
    
    @IBOutlet weak var achieveImg: UIImageView!
    
    @IBOutlet weak var details: UILabel!
    
    var detailString : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        detailString = AchievementModel.detail
        details.text = detailString
        achieveImg.image = UIImage(named: AchievementModel.ImageName)
    }
    
    
}
