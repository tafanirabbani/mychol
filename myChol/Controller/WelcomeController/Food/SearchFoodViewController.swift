//
//  SearchFoodViewController.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 29/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import UIKit
import RealmSwift

class SearchFoodViewController: UIViewController {

    var loading : UIActivityIndicatorView = UIActivityIndicatorView()
    var FoodListSearch : [Int] = []
    @IBOutlet weak var searchFood: UISearchBar!
    @IBOutlet weak var foodSearchTable: UITableView!
    
    override func viewDidLoad() {
        
        self.loading.center = self.view.center
        self.loading.hidesWhenStopped = true
        self.loading.style = UIActivityIndicatorView.Style.gray
        self.view.addSubview(self.loading)
        super.viewDidLoad()
    }
    

}

extension SearchFoodViewController:UITableViewDataSource,UITableViewDelegate{
   
    func run(after seconds : Int, completion : @escaping () -> Void ){
        let deadling = DispatchTime.now() + .seconds(seconds)
        DispatchQueue.main.asyncAfter(deadline: deadling){
            completion()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FoodListModel.foodList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let food = FoodListModel.foodList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "foodSearch") as! FoodSearchTableViewCell
        cell.setExercise(food: food)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = tableView.indexPathForSelectedRow
//        print(FoodListModel.foodList[(index?.row)!])
        
        let makanan : SearchFood.Food = FoodListModel.foodList[(index?.row)!]
        let sc : GetFood = GetFood()
        let id = Int(makanan.food_id!)
//        print(id)
        loading.startAnimating()
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        sc.getFood(id: String(id!))
        run(after: 2) {
            self.loading.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
            self.addToRealm()
        }
        
    }
    func addToRealm(){
        let exc : RealmFoodModel = RealmFoodModel()
        exc.day = DateModel.date
        exc.month = DateModel.month
        exc.year = DateModel.year
        let foodDetail = FoodModel.food
        exc.food_id = foodDetail?.id
        exc.name = foodDetail?.food_name
        exc.cholesterol = foodDetail?.serving?.cholesterol
        exc.fiber = foodDetail?.serving?.fiber
        exc.sat_fat = foodDetail?.serving?.sat_fat
        exc.trans_fat = foodDetail?.serving?.trans_fat
        exc.food_type = FoodDetailModel.type
//        exc.type =
        // Get the default Realm
        let realm = try! Realm()
        // Persist your data easily
        try! realm.write {
            realm.add(exc)
        }
        _ = navigationController?.popViewController(animated: true)
    }
}

extension SearchFoodViewController: UISearchBarDelegate{
    
//    func run(after seconds : Int, completion : @escaping () -> Void ){
//        let deadling = DispatchTime.now() + .seconds(seconds)
//        DispatchQueue.main.asyncAfter(deadline: deadling){
//            completion()
//        }
//    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
//       print(searchBar.text)
        let sc : SearchFood = SearchFood()
        self.loading.startAnimating()
        sc.searchFood(food : searchFood.text!,table: foodSearchTable)
        UIApplication.shared.beginIgnoringInteractionEvents()
        run(after: 2){
            self.loading.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
            self.foodSearchTable.reloadData()
        }
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        FoodListModel.foodList = []
        searchBar.text = ""
        self.foodSearchTable.reloadData()
    }
}
