//
//  AddFoodViewController.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 29/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import UIKit
import RealmSwift

class AddFoodViewController: UIViewController {

    @IBOutlet weak var noFoodTitle: UILabel!
    @IBOutlet weak var foodTable: UITableView!
    @IBOutlet weak var foodExistView: UIView!
    @IBOutlet weak var noFoodView: UIView!
    @IBOutlet weak var cholesterol: UILabel!
    @IBOutlet weak var trans_fat: UILabel!
    @IBOutlet weak var sat_fat: UILabel!
    @IBOutlet weak var sat_fat_progressBar: UIProgressView!
    @IBOutlet weak var trans_fat_progressBar: UIProgressView!
    @IBOutlet weak var cholesterol_progress_bar: UIProgressView!
//
    let maxSaturated = 13.0
    let maxTrans = 38.0
    let maxCholesterol = 200.0
    var sat = 0.0
    var trans = 0.0
    var chol = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        noFoodTitle.text = "You have no "+FoodDetailModel.type + " yet"
        let realm = try! Realm()
        
        let exerciseList = realm.objects(RealmFoodModel.self).filter("food_type = '\(FoodDetailModel.type)' AND day = \(DateModel.date) AND month == \(DateModel.month) AND year == \(DateModel.year)")
//        print(exerciseList)
        
        if exerciseList.count > 0{
            noFoodView.isHidden = true
            foodExistView.isHidden = false
        }else{
            noFoodView.isHidden = false
            foodExistView.isHidden = true
        }
        var tempFood : [FoodToday] = []
        chol = 0
        trans = 0
        sat = 0
        for  food in exerciseList{
            let f : FoodToday = FoodToday(
                name: food.name!, food_id: food.food_id!, cholesterol: food.cholesterol!, sat_fat: food.cholesterol!, trans_fat: String(0), fiber: food.fiber!)
            chol = chol + Double(food.cholesterol!)!
            trans = trans + Double(food.fiber!)!
            sat = sat + Double(food.sat_fat!)!
            tempFood.append(f)
        }
        FoodTodayModel.list = tempFood
        updateProgressBar()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        noFoodTitle.text = "You have no "+FoodDetailModel.type + " yet"
        let realm = try! Realm()
        
        let exerciseList = realm.objects(RealmFoodModel.self).filter("food_type = '\(FoodDetailModel.type)' AND day = \(DateModel.date) AND month == \(DateModel.month) AND year == \(DateModel.year)")
        //        print(exerciseList)
        
        if exerciseList.count > 0{
            noFoodView.isHidden = true
            foodExistView.isHidden = false
        }else{
            noFoodView.isHidden = false
            foodExistView.isHidden = true
        }
        var tempFood : [FoodToday] = []
        chol = 0
        trans = 0
        sat = 0
        for  food in exerciseList{
            let f : FoodToday = FoodToday(
                name: food.name!, food_id: food.food_id!, cholesterol: food.cholesterol!, sat_fat: food.sat_fat!, trans_fat: String(0), fiber: food.fiber!)
            chol = chol + Double(food.cholesterol!)!
            trans = trans + Double(food.fiber!)!
            sat = sat + Double(food.sat_fat!)!
            tempFood.append(f)
        }
        FoodTodayModel.list = tempFood
        foodTable.reloadData()
        updateProgressBar()
    }
    func updateProgressBar(){
        cholesterol.text = NSString(format: "%.2f grams Cholesterol", chol) as String
        trans_fat.text = NSString(format: "%.2f grams Fiber", trans) as String
        sat_fat.text = NSString(format: "%.2f grams Saturated Fat", sat) as String
        sat_fat_progressBar.progress = Float(sat / maxSaturated)
        trans_fat_progressBar.progress = Float(trans/maxTrans)
        cholesterol_progress_bar.progress = Float(chol/maxCholesterol)
    }
}

extension AddFoodViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FoodTodayModel.list.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let f = FoodTodayModel.list[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "addFoodCell") as! AddFoodCell
        cell.setExercise(food: f)
        return cell
    }
}
