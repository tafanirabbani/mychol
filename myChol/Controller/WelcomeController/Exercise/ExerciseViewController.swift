//
//  ExerciseViewController.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 29/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import UIKit
import RealmSwift
class ExerciseViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var ExerciseExistView: UIView!
    
    @IBOutlet weak var noExerciseVIew: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        exerciseList = createDummy()
//        createDummy()
        // Do any additional setup after loading the view.
//        ExerciseList.exercise = createDummy()
        
        let realm = try! Realm()
        
        let exerciseList = realm.objects(RealmExerciseModel.self).filter("day = \(DateModel.date) AND month = \(DateModel.month) AND year = \(DateModel.year)")
//        print(exerciseList.count)
        if exerciseList.count > 0{
            noExerciseVIew.isHidden = true
            ExerciseExistView.isHidden = false
        }else{
            noExerciseVIew.isHidden = false
            ExerciseExistView.isHidden = true
        }
        var tempExer : [Exercise] = []
        for exercise in exerciseList{
            let exc : Exercise = Exercise(title: exercise.name!)
            tempExer.append(exc)
        }
        ExerciseList.exercise = tempExer
    }
    
    func createDummy() -> [Exercise] {
        var tempExer : [Exercise] = []
        
        let a1 = Exercise(title: "Swimming")
        let a2 = Exercise(title: "Running")
        let a3 = Exercise(title: "Football")
        let a4 = Exercise(title: "Yoga")
        tempExer.append(a1)
        tempExer.append(a2)
        tempExer.append(a3)
        tempExer.append(a4)
        return tempExer
    }

}

extension ExerciseViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ExerciseList.exercise.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let exercise = ExerciseList.exercise[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExerciseCell") as! ExerciseTableViewCell
        cell.setExercise(exercise: exercise)
        return cell
    }
}
