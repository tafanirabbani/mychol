//
//  AddExerciseViewController.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 29/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import UIKit
import RealmSwift

class AddExerciseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func addToRealm(name : String){
        let exc : RealmExerciseModel = RealmExerciseModel()
        exc.day = DateModel.date
        exc.month = DateModel.month
        exc.year = DateModel.year
        exc.name = name
    
        // Get the default Realm
        let realm = try! Realm()
        // Persist your data easily
        try! realm.write {
            realm.add(exc)
        }
        _ = navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func addRunning(_ sender: Any) {
        addToRealm(name: "Running")
    }
    @IBAction func addWalking(_ sender: Any) {
         addToRealm(name: "Walking")
    }
    @IBAction func addYoga(_ sender: Any) {
         addToRealm(name: "Yoga")
    }
    @IBAction func addSwimming(_ sender: Any) {
         addToRealm(name: "Swimming")
    }
    @IBAction func addFootball(_ sender: Any) {
         addToRealm(name: "Football")
    }
    @IBAction func addBasket(_ sender: Any) {
         addToRealm(name: "BasketBall")
    }
    @IBAction func addOther(_ sender: Any) {
         addToRealm(name: "Other")
    }
}
