//
//  BloodPressureController.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 24/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import Foundation
import UIKit

class BloodPressureController : UIViewController{
    

    @IBOutlet weak var bp_1: UITextField!
    @IBOutlet weak var bp_2: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(UserModel.username)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        let awal :Int? = Int(bp_1.text ?? "1") ?? 0
        let akhir :Int? = Int(bp_2.text ?? "1") ?? 0
        
        UserModel.bloodPressure1 = awal ?? 0
        UserModel.bloodPressure2 = akhir ?? 0
        print(awal,akhir)
    }
}
