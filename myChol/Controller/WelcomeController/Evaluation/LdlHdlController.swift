//
//  LdlHdlController.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 24/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import Foundation
import UIKit

class LdlHdlController : UIViewController{
    
//    @IBOutlet weak var hdlTextField: UITextField!
    
    @IBOutlet weak var hdlTextField: UITextField!
    @IBOutlet weak var ldlTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        print(UserModel.username)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        let ldl : Int = Int(ldlTextField.text ?? "0") ?? 0
        let hdl : Int = Int(hdlTextField.text ?? "0") ?? 0
        
        UserModel.ldlLevel = ldl
        UserModel.hdlLevel = hdl
        
    }
}
