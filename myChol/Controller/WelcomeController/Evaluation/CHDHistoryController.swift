//
//  CHDHistoryController.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 24/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import Foundation
import UIKit

class CHDHistoryController : UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(UserModel.username)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "chd_yes"{
            print("yes")
            UserModel.chdHistory = true
        }else if segue.identifier == "chd_no"{
            print("no")
            UserModel.chdHistory = false
        }else{
            print("error")
            
        }
        
    }
}
