//
//  NewUserController.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 24/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import Foundation
import UIKit

class AreYouSmokerController : UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(UserModel.username)
    
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "yes_smoker"{
            print("yes")
            UserModel.smoker = true
        }else if segue.identifier == "no_smoker"{
            print("no")
            UserModel.smoker = false
        }else{
            print("error")
            
        }
        
    }
}
