//
//  NewUserController.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 24/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import Foundation
import UIKit

class NewUserController : UIViewController{
    
    
    @IBOutlet weak var userNameTxt_evaluation: UITextField!
    @IBOutlet weak var ageTxt_evaluation: UITextField!
    
    @IBOutlet weak var genderTxt_evaluation: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(UserModel.username)
//        print(userNameTxt_evaluation.text)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        let userName:String = userNameTxt_evaluation.text ?? ""
        let age:Int = Int(ageTxt_evaluation!.text ?? "0") ?? 0
        let gender:String = genderTxt_evaluation.text ?? ""
        
        UserModel.username = userName
        UserModel.age = age
        UserModel.gender = gender

        
    }
}
