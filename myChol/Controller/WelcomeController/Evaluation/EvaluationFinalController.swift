//
//  EvaluationFinalController.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 24/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class EvaluationFinalController : UIViewController{
    
    
    @IBOutlet weak var bpView: UILabel!
    @IBOutlet weak var chdView: UILabel!
    @IBOutlet weak var hdlView: UILabel!
    @IBOutlet weak var ldlView: UILabel!
    @IBOutlet weak var smokerView: UILabel!
    @IBOutlet weak var genderView: UILabel!
    @IBOutlet weak var ageView: UILabel!
    @IBOutlet weak var nameView: UILabel!
    @IBOutlet weak var ldlStatus: UIButton!
    @IBOutlet weak var bloodStatus: UIButton!
    @IBOutlet weak var primaryRisksStatus: UIButton!
    @IBOutlet weak var tenYearStatus: UIButton!
    
    var username : String?
    var age : Int?
    var gender : String?
    var smoker : Bool = false
    var CHDHistory : Bool = false
    var bloodp1 : Int?
    var bloodp2 : Int?
    var ldlLevel : Int?
    var hdlLevel : Int?
    var primaryRiks : Int = 0
    var tenYearScore : Int = 0
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "finishEvaluation"{
            let userRealm = RealmUserModel()
            userRealm.username = username
            userRealm.age = String(age!)
            userRealm.gender = gender
            userRealm.smoker = String(smoker)
            userRealm.chdHistory = String(CHDHistory)
            userRealm.bloodPressure1 = String(bloodp1!)
            userRealm.bloodPressure2 = String(bloodp2!)
            userRealm.ldlLevel = String(ldlLevel!)
            userRealm.hdlLevel = String(hdlLevel!)
            
            
            // Get the default Realm
            let realm = try! Realm()
            // Persist your data easily
            try! realm.write {
                realm.add(userRealm)
            }
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        username = UserModel.username
        age = UserModel.age
        gender = UserModel.gender
        smoker = UserModel.smoker
        CHDHistory = UserModel.chdHistory
        bloodp1 = UserModel.bloodPressure1
        bloodp2 = UserModel.bloodPressure2
//        print(bloodp1,bloodp2)
        ldlLevel = UserModel.ldlLevel
        hdlLevel = UserModel.hdlLevel
        
        if gender == "Male"{
          tenYearScore = tenYearCountMale(ldlLevel: ldlLevel!, hdlLevel: hdlLevel!, age: age!, smoker: smoker, bloodp1: bloodp1!)
        }else if gender == "Female"{
          tenYearScore = tenYearCountFemale(ldlLevel: ldlLevel!, hdlLevel: hdlLevel!, age: age!, smoker: smoker, bloodp1: bloodp1!)
        }
        
        loadDataUser()
        countPrimaryRisk()
//        print(primaryRiks)
        updateButton()
    }

    func updateButton(){
        updateLDLButton()
        updateBloodButton()
        updatePrimaryRiskButton()
        updateTenYearButton()
    }
    
    func updateTenYearButton( ){
        if tenYearScore > 20{
            tenYearStatus.setTitle("   Your 10-year  Risks Assesment             \(tenYearScore)%", for: UIControl.State.normal)
            tenYearStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            tenYearStatus.setBackgroundImage(UIImage(named: "redButton"), for: UIControl.State.normal)
        }else if tenYearScore > 10{
            tenYearStatus.setTitle("   Your 10-year  Risks Assesment              \(tenYearScore)%", for: UIControl.State.normal)
            tenYearStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            tenYearStatus.setBackgroundImage(UIImage(named: "yellowButton"), for: UIControl.State.normal)
        }else{
            tenYearStatus.setTitle("   Your 10-year  Risks Assesment              \(tenYearScore)%", for: UIControl.State.normal)
            tenYearStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            tenYearStatus.setBackgroundImage(UIImage(named: "square_btn"), for: UIControl.State.normal)
        }
    }
    func countPrimaryRisk(){
        switch gender {
        case "Male":
            if age!>45{
                primaryRiks = primaryRiks + 1
            }
        case "Female":
            if age!>55{
                primaryRiks = primaryRiks + 1
            }
        default: break
        }
        if smoker{
            primaryRiks = primaryRiks + 1
        }
        if CHDHistory{
            primaryRiks = primaryRiks + 1
        }
        if (bloodp1!>130)&&(bloodp2!>80){
            primaryRiks = primaryRiks + 1
        }
    }
    func loadDataUser(){
        nameView.text = username
        ageView.text = String(age ?? 0)
        genderView.text = gender
        if smoker{
            smokerView.text = "Yes"
        }else{
            smokerView.text = "No"
        }
        if CHDHistory ?? false {
            chdView.text = "Yes"
        }else{
            chdView.text = "No"
        }
        if (bloodp1 != nil) && (bloodp2 != nil) {
            let a = bloodp1!
            let b = bloodp2!
            if a == 0 || b == 0 {
                bpView.text = "None"
            }else{
                bpView.text = "\(Int(a))/\(Int(b)) mmHG"
            }
        }else{
            bpView.text = "None"
        }
        if ldlLevel != nil{
            let a = ldlLevel!
            if a == 0{
                ldlView.text = "None"
            }else{
                ldlView.text = "\(Int(ldlLevel!)) mg/dl"
            }
        }else{
            ldlView.text = "None"
        }
        if hdlLevel != nil{
            let a = hdlLevel!
            if a == 0{
                hdlView.text = "None"
            }else{
                hdlView.text = "\(Int(hdlLevel!)) mg/dl"
            }
        }else{
            hdlView.text = "None"
        }
        
    }
    
    func updateLDLButton(){
        if ldlLevel! > 189{
            ldlStatus.setTitle("Your LDL Level is              High", for: UIControl.State.normal)
            ldlStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            ldlStatus.setBackgroundImage(UIImage(named: "redButton"), for: UIControl.State.normal)
        }else if ldlLevel! > 159{
            ldlStatus.setTitle("Your LDL Level is    Borderline High", for: UIControl.State.normal)
            ldlStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            ldlStatus.setBackgroundImage(UIImage(named: "yellowButton"), for: UIControl.State.normal)
        }else if ldlLevel! == 0{
            ldlStatus.setTitle("Your LDL Level is              None", for: UIControl.State.normal)
            ldlStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            ldlStatus.setBackgroundImage(UIImage(named: "greyButton"), for: UIControl.State.normal)
        }
    }
    
    func updateBloodButton()  {
        if (bloodp1! > 130) || (bloodp2! > 79 && bloodp2!<91){
            
            bloodStatus.setTitle("     Your Blood  Pressure Stage is              High", for: UIControl.State.normal)
            bloodStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            bloodStatus.setBackgroundImage(UIImage(named: "redButton"), for: UIControl.State.normal)
        } else if (bloodp1! > 119) && (bloodp2! < 80){
            bloodStatus.setTitle("     Your Blood  Pressure Stage is              Elevated", for: UIControl.State.normal)
            bloodStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            bloodStatus.setBackgroundImage(UIImage(named: "yellowButton"), for: UIControl.State.normal)
        }else if bloodp1 == 0 || bloodp2 == 0{
            bloodStatus.setTitle("     Your Blood  Pressure Stage is              None", for: UIControl.State.normal)
            bloodStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            bloodStatus.setBackgroundImage(UIImage(named: "greyButton"), for: UIControl.State.normal)
        }
    }
    func updatePrimaryRiskButton(){
        if primaryRiks == 1{
            primaryRisksStatus.setTitle("Your Primary Risk              is                1", for: UIControl.State.normal)
            primaryRisksStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            primaryRisksStatus.setBackgroundImage(UIImage(named: "yellowButton"), for: UIControl.State.normal)
        }else if primaryRiks > 1{
            primaryRisksStatus.setTitle("Your Primary Risk              is                \(primaryRiks)", for: UIControl.State.normal)
            primaryRisksStatus.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            primaryRisksStatus.setBackgroundImage(UIImage(named: "redButton"), for: UIControl.State.normal)
        }
    }
    
    func tenYearCountMale(ldlLevel:Int,hdlLevel:Int,age:Int,smoker:Bool,bloodp1:Int) -> Int{
        
        var point = 0
        let total = ldlLevel + hdlLevel
        var use = 0
        if age > 75{
            use = 0
        }else if age > 70 {
            use = 1
        }else if age>65{
            use = 2
        }else if age>60{
            use = 3
        }else if age>55{
            use = 4
        }else if age>50{
            use = 5
        }else if age>45{
            use = 6
        }else if age>40{
            use = 7
        }else if age>35{
            use = 8
        }else{
            use = 9
        }
        
        
        switch use {
        case 0:
            point = point + 13
            if total > 240{
                point = point + 1
            }
            if smoker{
                point = point + 1
            }
        case 1:
            point = point + 12
            if total > 240{
                point = point + 1
            }
            if smoker{
                point = point + 1
            }
        case 2:
            point = point + 11
            if total > 280{
                point = point + 3
            }else if total > 240{
                point = point + 2
            }else if total > 200{
                point = point + 1
            }else if total > 160{
                point = point + 1
            }
            
            if smoker{
                point = point + 1
            }
        case 3:
            point = point + 10
            if total > 280{
                point = point + 3
            }else if total > 240{
                point = point + 2
            }else if total > 200{
                point = point + 1
            }else if total > 160{
                point = point + 1
            }
            
            if smoker{
                point = point + 1
            }
        case 4:
            point = point + 8
            if total > 280{
                point = point + 5
            }else if total > 240{
                point = point + 4
            }else if total > 200{
                point = point + 3
            }else if total > 160{
                point = point + 2
            }
            if smoker{
                point = point + 3
            }
        case 5:
            point = point + 6
            if total > 280{
                point = point + 5
            }else if total > 240{
                point = point + 4
            }else if total > 200{
                point = point + 3
            }else if total > 160{
                point = point + 2
            }
            
            if smoker{
                point = point + 3
            }
        case 6:
            point = point + 3
            if total > 280{
                point = point + 8
            }else if total > 240{
                point = point + 6
            }else if total > 200{
                point = point + 5
            }else if total > 160{
                point = point + 3
            }
            if smoker{
                point = point + 5
            }
        case 7:
            point = point + 0
            point = point + 3
            if total > 280{
                point = point + 8
            }else if total > 240{
                point = point + 6
            }else if total > 200{
                point = point + 5
            }else if total > 160{
                point = point + 3
            }
            
            if smoker{
                point = point + 5
            }
        case 8:
            point = point - 4
            if total > 280{
                point = point + 11
            }else if total > 240{
                point = point + 7
            }else if total > 200{
                point = point + 4
            }else if total > 160{
                point = point + 0
            }
            if smoker{
                point = point + 8
            }
        case 9:
            point = point - 9
            if total > 280{
                point = point + 11
            }else if total > 240{
                point = point + 7
            }else if total > 200{
                point = point + 4
            }else if total > 160{
                point = point + 0
            }
            if smoker{
                point = point + 8
            }
        default:
            break
        }
        
        if hdlLevel > 60{
            point = point - 1
        }else if hdlLevel > 50{
            
        }else if hdlLevel > 40{
            point = point + 1
        }else{
            point = point + 2
        }
        if bloodp1 > 160{
            point = point + 2
        }else if bloodp1 > 130{
            point = point + 1
        }
        if point > 16{
            return 30
        } else if point > 15{
            return 25
        }else if point > 14{
            return 20
        }else if point > 13{
            return 16
        }else if point > 12{
            return 12
        }else if point > 11{
            return 10
        }else if point > 10{
            return 8
        }else if point > 9{
            return 6
        }else if point > 8{
            return 5
        }else if point > 7{
            return 4
        }else if point > 6{
            return 3
        }else if point > 4{
            return 2
        }else {
            return 1
        }
    }
    func tenYearCountFemale(ldlLevel : Int, hdlLevel : Int, age : Int?, smoker : Bool, bloodp1 : Int) -> Int{
        var point = 0
        
        let total = ldlLevel + hdlLevel
        var use = 0
        if age! > 75{
            use = 0
        }else if age! > 70 {
            use = 1
        }else if age!>65{
            use = 2
        }else if age!>60{
            use = 3
        }else if age!>55{
            use = 4
        }else if age!>50{
            use = 5
        }else if age!>45{
            use = 6
        }else if age!>40{
            use = 7
        }else if age!>35{
            use = 8
        }else{
            use = 9
        }
        
        
        switch use {
        case 0:
            point = point + 13
            if total > 240{
                point = point + 1
            }
            if smoker{
                point = point + 1
            }
        case 1:
            point = point + 12
            if total > 240{
                point = point + 1
            }
            if smoker{
                point = point + 1
            }
        case 2:
            point = point + 11
            if total > 280{
                point = point + 3
            }else if total > 240{
                point = point + 2
            }else if total > 200{
                point = point + 1
            }else if total > 160{
                point = point + 1
            }
            
            if smoker{
                point = point + 1
            }
        case 3:
            point = point + 10
            if total > 280{
                point = point + 3
            }else if total > 240{
                point = point + 2
            }else if total > 200{
                point = point + 1
            }else if total > 160{
                point = point + 1
            }
            
            if smoker{
                point = point + 1
            }
        case 4:
            point = point + 8
            if total > 280{
                point = point + 5
            }else if total > 240{
                point = point + 4
            }else if total > 200{
                point = point + 3
            }else if total > 160{
                point = point + 2
            }
            if smoker{
                point = point + 3
            }
        case 5:
            point = point + 6
            if total > 280{
                point = point + 5
            }else if total > 240{
                point = point + 4
            }else if total > 200{
                point = point + 3
            }else if total > 160{
                point = point + 2
            }
            
            if smoker{
                point = point + 3
            }
        case 6:
            point = point + 3
            if total > 280{
                point = point + 8
            }else if total > 240{
                point = point + 6
            }else if total > 200{
                point = point + 5
            }else if total > 160{
                point = point + 3
            }
            if smoker{
                point = point + 5
            }
        case 7:
            point = point + 0
            point = point + 3
            if total > 280{
                point = point + 8
            }else if total > 240{
                point = point + 6
            }else if total > 200{
                point = point + 5
            }else if total > 160{
                point = point + 3
            }
            
            if smoker{
                point = point + 5
            }
        case 8:
            point = point - 4
            if total > 280{
                point = point + 11
            }else if total > 240{
                point = point + 7
            }else if total > 200{
                point = point + 4
            }else if total > 160{
                point = point + 0
            }
            if smoker{
                point = point + 8
            }
        case 9:
            point = point - 9
            if total > 280{
                point = point + 11
            }else if total > 240{
                point = point + 7
            }else if total > 200{
                point = point + 4
            }else if total > 160{
                point = point + 0
            }
            if smoker{
                point = point + 8
            }
        default:
            break
        }
        
        if hdlLevel > 60{
            point = point - 1
        }else if hdlLevel > 50{
            
        }else if hdlLevel > 40{
            point = point + 1
        }else{
            point = point + 2
        }
        if bloodp1 > 160{
            point = point + 2
        }else if bloodp1 > 130{
            point = point + 1
        }
        if point > 16{
            return 30
        } else if point > 15{
            return 25
        }else if point > 14{
            return 20
        }else if point > 13{
            return 16
        }else if point > 12{
            return 12
        }else if point > 11{
            return 10
        }else if point > 10{
            return 8
        }else if point > 9{
            return 6
        }else if point > 8{
            return 5
        }else if point > 7{
            return 4
        }else if point > 6{
            return 3
        }else if point > 4{
            return 2
        }else {
            return 1
        }
        return 1
    }
}
