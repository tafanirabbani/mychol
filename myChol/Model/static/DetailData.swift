//
//  dataDetail.swift
//  myChol
//
//  Created by Malvin Santoso on 26/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import Foundation
import UIKit


struct DetailData{
    static var titleDetail:String = ""
    static var detail:String = ""
    static var namaGambar:String = ""
    
}
