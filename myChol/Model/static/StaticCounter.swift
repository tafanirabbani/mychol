//
//  UserModel.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 24/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import Foundation
import UIKit

struct StaticCounter{
    
    static let maxSaturated = 13.0
    static let maxTrans = 20.0
    static let maxCholesterol = 200.0
    static var sat = 0.0
    static var trans = 0.0
    static var chol = 0.0
    
}

