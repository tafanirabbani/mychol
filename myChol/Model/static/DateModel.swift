//
//  DateModel.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 29/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import Foundation
import UIKit

struct DateModel{
    
    static var date:Int = 0
    static var month:Int = 0
    static var year:Int = 0

}
