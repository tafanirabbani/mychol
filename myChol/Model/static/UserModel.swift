//
//  UserModel.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 24/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import Foundation
import UIKit

struct UserModel{
    
    static var username:String = ""
    static var age:Int = 20
    static var gender:String = "Male"
    static var smoker:Bool = true
    static var chdHistory:Bool = true
    static var bloodPressure1:Int = 0
    static var bloodPressure2:Int = 0
    static var ldlLevel:Int = 0
    static var hdlLevel:Int = 0
    
}

