//
//  RealmUserModel.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 25/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import Foundation
import RealmSwift


class RealmPhotoProfile : Object{
    @objc dynamic var path:String?
}
