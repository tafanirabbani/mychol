//
//  RealmExerciseModel.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 29/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import Foundation
import RealmSwift
class RealmExerciseModel : Object{
     @objc dynamic var name:String?
     @objc dynamic var day:Int = 0
     @objc dynamic var month:Int = 0
     @objc dynamic var year:Int = 0
}
