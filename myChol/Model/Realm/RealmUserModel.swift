//
//  RealmUserModel.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 25/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import Foundation
import RealmSwift


class RealmUserModel : Object{
    @objc dynamic var username:String?
    @objc dynamic var age:String?
    @objc dynamic var gender:String?
    @objc dynamic var smoker:String?
    @objc dynamic var chdHistory:String?
    @objc dynamic var bloodPressure1:String?
    @objc dynamic var bloodPressure2:String?
    @objc dynamic var ldlLevel:String?
    @objc dynamic var hdlLevel:String?
}
