//
//  Excersie.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 29/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import Foundation
class FoodToday {
    var name:String?
    var food_id:String?
    var cholesterol : String?
    var sat_fat : String?
    var trans_fat : String?
    var fiber : String?
    
    init(name: String, food_id:String,cholesterol:String,sat_fat:String,trans_fat:String,fiber:String) {
        self.name = name
        self.food_id = food_id
        self.cholesterol = cholesterol
        self.sat_fat = sat_fat
        self.trans_fat = trans_fat
        self.fiber = fiber
    }
}
