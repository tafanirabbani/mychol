//
//  RoundedButton.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 23/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import UIKit

class BaseRoundedButton: UIButton {
    var btnBackgroundColor: UIColor {
        get {
            return UIColor(red:0.35, green:0.44, blue:0.69, alpha:1)
        }
    }
    
    private var disabledBtnBackgroundColor: UIColor {
        get {
            return UIColor(red:0.95, green:0.95, blue:0.95, alpha:1)
        }
    }
    
    var currentState: BtnState = .enabled
    
    func setupView() {
//        self.titleLabel?.font = KEYS.FONTS.getFont(font: .semiBold, size: 14)
        self.layer.cornerRadius = self.frame.size.height / 2
        
        self.backgroundColor = btnBackgroundColor
        self.layer.shadowOffset = CGSize(width: 0, height: 4)
        self.layer.shadowColor = UIColor(red:0.69, green:0.77, blue:0.87, alpha:0.9).cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 12
        self.isUserInteractionEnabled = true
        self.setTitleColor(.white, for: .normal)
    }
    
    private func disableBtn() {
        self.backgroundColor = disabledBtnBackgroundColor
        self.layer.shadowOpacity = 0
        self.setTitleColor(UIColor(red: 0.62, green: 0.62, blue: 0.62, alpha: 1), for: .normal)
    }
    
    private func highlightBtn() {
        self.backgroundColor = UIColor.white
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowColor = UIColor(red:0.35, green:0.41, blue:0.69, alpha:0.3).cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 5
        
        self.setTitleColor(btnBackgroundColor, for: .normal)
    }
    
    func setBtnState(_ state: BtnState) {
        self.currentState = state
        
        switch state {
        case .enabled:
            setupView()
        case .disabled:
            disableBtn()
        case .highlighted:
            highlightBtn()
        }
    }
    
    enum BtnState {
        case enabled
        case disabled
        case highlighted
    }
}
