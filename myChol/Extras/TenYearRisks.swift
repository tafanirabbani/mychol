//
//  tenYearRisks.swift
//  myChol
//
//  Created by Muhammad Tafani Rabbani on 25/04/19.
//  Copyright © 2019 Muhammad Tafani Rabbani. All rights reserved.
//

import Foundation

class TenYearRisks {
    
    func tenYearCountMale(ldlLevel:Int,hdlLevel:Int,age:Int,smoker:Bool,bloodp1:Int) -> Int{
        
        var point = 0
        let total = ldlLevel + hdlLevel
        var use = 0
        if age > 75{
            use = 0
        }else if age > 70 {
            use = 1
        }else if age>65{
            use = 2
        }else if age>60{
            use = 3
        }else if age>55{
            use = 4
        }else if age>50{
            use = 5
        }else if age>45{
            use = 6
        }else if age>40{
            use = 7
        }else if age>35{
            use = 8
        }else{
            use = 9
        }
        
        
        switch use {
        case 0:
            point = point + 13
            if total > 240{
                point = point + 1
            }
            if smoker{
                point = point + 1
            }
        case 1:
            point = point + 12
            if total > 240{
                point = point + 1
            }
            if smoker{
                point = point + 1
            }
        case 2:
            point = point + 11
            if total > 280{
                point = point + 3
            }else if total > 240{
                point = point + 2
            }else if total > 200{
                point = point + 1
            }else if total > 160{
                point = point + 1
            }
            
            if smoker{
                point = point + 1
            }
        case 3:
            point = point + 10
            if total > 280{
                point = point + 3
            }else if total > 240{
                point = point + 2
            }else if total > 200{
                point = point + 1
            }else if total > 160{
                point = point + 1
            }
            
            if smoker{
                point = point + 1
            }
        case 4:
            point = point + 8
            if total > 280{
                point = point + 5
            }else if total > 240{
                point = point + 4
            }else if total > 200{
                point = point + 3
            }else if total > 160{
                point = point + 2
            }
            if smoker{
                point = point + 3
            }
        case 5:
            point = point + 6
            if total > 280{
                point = point + 5
            }else if total > 240{
                point = point + 4
            }else if total > 200{
                point = point + 3
            }else if total > 160{
                point = point + 2
            }
            
            if smoker{
                point = point + 3
            }
        case 6:
            point = point + 3
            if total > 280{
                point = point + 8
            }else if total > 240{
                point = point + 6
            }else if total > 200{
                point = point + 5
            }else if total > 160{
                point = point + 3
            }
            if smoker{
                point = point + 5
            }
        case 7:
            point = point + 0
            point = point + 3
            if total > 280{
                point = point + 8
            }else if total > 240{
                point = point + 6
            }else if total > 200{
                point = point + 5
            }else if total > 160{
                point = point + 3
            }
            
            if smoker{
                point = point + 5
            }
        case 8:
            point = point - 4
            if total > 280{
                point = point + 11
            }else if total > 240{
                point = point + 7
            }else if total > 200{
                point = point + 4
            }else if total > 160{
                point = point + 0
            }
            if smoker{
                point = point + 8
            }
        case 9:
            point = point - 9
            if total > 280{
                point = point + 11
            }else if total > 240{
                point = point + 7
            }else if total > 200{
                point = point + 4
            }else if total > 160{
                point = point + 0
            }
            if smoker{
                point = point + 8
            }
        default:
            break
        }
        
        if hdlLevel > 60{
            point = point - 1
        }else if hdlLevel > 50{
            
        }else if hdlLevel > 40{
            point = point + 1
        }else{
            point = point + 2
        }
        if bloodp1 > 160{
            point = point + 2
        }else if bloodp1 > 130{
            point = point + 1
        }
        if point > 16{
            return 30
        } else if point > 15{
          return 25
        }else if point > 14{
            return 20
        }else if point > 13{
            return 16
        }else if point > 12{
            return 12
        }else if point > 11{
            return 10
        }else if point > 10{
            return 8
        }else if point > 9{
            return 6
        }else if point > 8{
            return 5
        }else if point > 7{
            return 4
        }else if point > 6{
            return 3
        }else if point > 4{
            return 2
        }else {
            return 1
        }
    }
    func tenYearCountFemale(ldlLevel : Int, hdlLevel : Int, age : Int?, smoker : Bool, bloodp1 : Int) -> Int{
        var point = 0
        
        let total = ldlLevel + hdlLevel
        var use = 0
        if age! > 75{
            use = 0
        }else if age! > 70 {
            use = 1
        }else if age!>65{
            use = 2
        }else if age!>60{
            use = 3
        }else if age!>55{
            use = 4
        }else if age!>50{
            use = 5
        }else if age!>45{
            use = 6
        }else if age!>40{
            use = 7
        }else if age!>35{
            use = 8
        }else{
            use = 9
        }
        
        
        switch use {
        case 0:
            point = point + 13
            if total > 240{
                point = point + 1
            }
            if smoker{
                point = point + 1
            }
        case 1:
            point = point + 12
            if total > 240{
                point = point + 1
            }
            if smoker{
                point = point + 1
            }
        case 2:
            point = point + 11
            if total > 280{
                point = point + 3
            }else if total > 240{
                point = point + 2
            }else if total > 200{
                point = point + 1
            }else if total > 160{
                point = point + 1
            }
            
            if smoker{
                point = point + 1
            }
        case 3:
            point = point + 10
            if total > 280{
                point = point + 3
            }else if total > 240{
                point = point + 2
            }else if total > 200{
                point = point + 1
            }else if total > 160{
                point = point + 1
            }
            
            if smoker{
                point = point + 1
            }
        case 4:
            point = point + 8
            if total > 280{
                point = point + 5
            }else if total > 240{
                point = point + 4
            }else if total > 200{
                point = point + 3
            }else if total > 160{
                point = point + 2
            }
            if smoker{
                point = point + 3
            }
        case 5:
            point = point + 6
            if total > 280{
                point = point + 5
            }else if total > 240{
                point = point + 4
            }else if total > 200{
                point = point + 3
            }else if total > 160{
                point = point + 2
            }
            
            if smoker{
                point = point + 3
            }
        case 6:
            point = point + 3
            if total > 280{
                point = point + 8
            }else if total > 240{
                point = point + 6
            }else if total > 200{
                point = point + 5
            }else if total > 160{
                point = point + 3
            }
            if smoker{
                point = point + 5
            }
        case 7:
            point = point + 0
            point = point + 3
            if total > 280{
                point = point + 8
            }else if total > 240{
                point = point + 6
            }else if total > 200{
                point = point + 5
            }else if total > 160{
                point = point + 3
            }
            
            if smoker{
                point = point + 5
            }
        case 8:
            point = point - 4
            if total > 280{
                point = point + 11
            }else if total > 240{
                point = point + 7
            }else if total > 200{
                point = point + 4
            }else if total > 160{
                point = point + 0
            }
            if smoker{
                point = point + 8
            }
        case 9:
            point = point - 9
            if total > 280{
                point = point + 11
            }else if total > 240{
                point = point + 7
            }else if total > 200{
                point = point + 4
            }else if total > 160{
                point = point + 0
            }
            if smoker{
                point = point + 8
            }
        default:
            break
        }
        
        if hdlLevel > 60{
            point = point - 1
        }else if hdlLevel > 50{
            
        }else if hdlLevel > 40{
            point = point + 1
        }else{
            point = point + 2
        }
        if bloodp1 > 160{
            point = point + 2
        }else if bloodp1 > 130{
            point = point + 1
        }
        if point > 16{
            return 30
        } else if point > 15{
            return 25
        }else if point > 14{
            return 20
        }else if point > 13{
            return 16
        }else if point > 12{
            return 12
        }else if point > 11{
            return 10
        }else if point > 10{
            return 8
        }else if point > 9{
            return 6
        }else if point > 8{
            return 5
        }else if point > 7{
            return 4
        }else if point > 6{
            return 3
        }else if point > 4{
            return 2
        }else {
            return 1
        }
        
    }
}
